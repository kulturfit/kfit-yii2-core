<?php

namespace kfit\core\helpers;

use Firebase\JWT\JWT;
use kfit\adm\components\Helper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\User;

/**
 * Clase Helper para ayudar a administrar los información de usuario
 *
 * @package kfit
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class UserHelper
{
    /**
     * Método encargado de verificar si el usuario tiene acceso a una ruta en especifica
     *
     * @param array $url Url en formato array para verificar
     * @param integer|User $user Usuario para verificar (Opcional)
     * @return boolean
     */
    public static function checkRouteFromUrl($url, $user = null)
    {
        $route = '';
        $params = [];
        $cont = 0;

        foreach ($url as $key => $value) {
            if ($cont == 0) {
                $route = $value;
            } else {
                $params[$key] = $value;
            }
            $cont++;
        }

        return YII_ENV_PROD ? Helper::checkRoute($route, $params, $user) : true;
    }

    /**
     * Entrega un listado de usuarios para usar en un dropDownList
     *
     * @return array
     */
    public static function getUsers()
    {
        $modelObject = \Yii::createObject(Yii::$app->user->identityClass);
        $data = $modelObject
            ::find([
                $modelObject::STATUS_COLUMN => $modelObject::STATUS_ACTIVE
            ])
            ->select(['user_id', 'username'])
            ->orderBy('username ASC')
            ->cache(3)
            ->asArray()
            ->all();
        return ArrayHelper::map($data, 'user_id', 'username');
    }

    /**
     * Método encargado de setear el nombre del usuario logueado
     *
     * @param string $name
     */
    public static function setName($name)
    {
        Yii::$app->session->set('name', $name);
    }

    /**
     * Método encargado de entregar el nombre del usuario logueado
     *
     * @return string Nombre del usuario
     */
    public static function getName()
    {
        return Yii::$app->session->get('name');
    }

    /**
     * Método encargado de setear el nombre completo del usuario logueado
     *
     * @param string $fullName
     */
    public static function setFullName($fullName)
    {
        Yii::$app->session->set('full_name', $fullName);
    }

    /**
     * Método encargado de entregar el nombre completo del usuario logueado
     *
     * @return string Nombre del usuario
     */
    public static function getFullName()
    {
        return Yii::$app->session->get('full_name');
    }

    /**
     * Método encargado de setear la foto del usuario logueado
     *
     * @param string $photo
     */
    public static function setPhoto($photo)
    {
        Yii::$app->session->set('photo', $photo);
    }

    /**
     * Método encargado de entregar la foto del usuario logueado
     *
     * @return string url de la foto
     */
    public static function getPhoto()
    {
        return Yii::$app->session->get('photo');
    }

    /**
     * Entrega el directorio publico de los assets del theme
     *
     * @return string
     */
    public static function getDirectoryThemeAsset()
    {
        return Yii::$app->assetManager->getPublishedUrl('@themes/assets');
    }

    /**
     * Encargado de retornar la ruta de la imagen por defecto para personas
     *
     * @return string
     */
    public static function getDefaultPhoto()
    {
        return self::getDirectoryThemeAsset() . "/images/default-photo.jpg";
    }

    /**
     * Encargado de retornar la ruta de la imagen por defecto para no foto
     *
     * @return string
     */
    public static function getNoPhoto()
    {
        return self::getDirectoryThemeAsset() . "/images/no_photo.jpg";
    }

    /**
     * Genera el token de renovación para JWT
     * 
     * @param string $type Tipo de token que se generará (acceso o renovación)
     * @param int $expiredTime Tiempo en UNIX Time en el cual expirará el token
     * 
     * @return string
     */
    public static function generateToken($type, $expiredTime)
    {
        // $identityClass = Yii::$app->user->identityClass;
        // $uuid = Yii::$app->request->headers->get('uuid', '');

        $token = new \app\models\app\AccessTokens();
        $token->access_token = Yii::$app->security->generateRandomString(64);
        $token->user_id = Yii::$app->user->id;
        $token->type = $type;
        // $token->active = AccessTokens

        if ($token->save()) {
            $privateKey = Yii::$app->params['privateKey'];
            $payload = array(
                "iss" => "www.kulturfit.com", //Identifica el proveedor de identidad que emitió el JWT 
                "aud" => 'kfit-coaching', //Identifica el objeto o usuario en nombre del cual fue emitido el JWT 
                "exp" => $expiredTime, //Identifica la marca temporal luego de la cual el JWT no tiene que ser aceptado.  
                "iat" => time(), //Identifica la marca temporal en qué el JWT fue emitido. 
                "nbf" => time(), //Identifica la marca temporal en que el JWT comienza a ser válido. EL JWT no tiene que ser aceptado si el token es utilizando antes de este tiempo.  
                'tok' => $token->access_token,
                'type' => $type
            );
            return JWT::encode($payload, $privateKey, 'RS512');
        } else {
            throw new \Exception('Error interno');
        }
    }
}
