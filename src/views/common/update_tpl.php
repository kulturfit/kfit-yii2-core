<div class="<?= $id ?>-update card">
    <div class="card-body">
        <div class="d-flex align-items-start justify-content-between mb-4">
            <h4>{{title}}</h4>
        </div>
        {{form}}
    </div>
</div>