<?php

namespace kfit\core;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use kfit\core\base\Bootstrap as BootstrapBase;
use yii\validators\CompareValidator;
use yii\validators\UniqueValidator;

/**
 * Clase cargadora de características para la aplicación.
 *
 * @package kfit
 * @subpackage yii2-base
 * @category Bootstrap
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 0.0.0
 */
class Bootstrap extends BootstrapBase
{
    public $moduleId = 'core';
    public $pieces = [
        'modules' => [
            'core' => \kfit\core\Module::class
        ],
        'components' => [
            'ui' => \kfit\core\helpers\UIHelper::class,
            'html' => \kfit\core\helpers\HtmlHelper::class,
            'strings' => \kfit\core\helpers\StringsHelper::class,
            'message' => \kfit\core\helpers\MessageHelper::class,
            'userHelper' => \kfit\core\helpers\UserHelper::class,
            'arrayHelper' => \kfit\core\helpers\ArrayHelper::class
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        Yii::$container->set(CompareValidator::class, TicmakersCompareValidator::class);
        Yii::$container->set(UniqueValidator::class, TicmakersUniqueValidator::class);

        /**
         * Validar para las webview
         */
        if(false){
            if ($app instanceof \yii\console\Application) {
                $app->getModule($this->moduleId)->controllerNamespace =
                    'kfit\core\commands';
            }
    
            $bundles = $app->getAssetManager()->bundles;
    
            if (!isset($bundles['yii\widgets\ActiveFormAsset'])) {
                $app->getAssetManager()->bundles['yii\widgets\ActiveFormAsset'] = [
                    'sourcePath' => '@vendor/kfit/yii2-core/src/assets',
                    'js' => [
                        'yii.activeForm.js',
                    ],
                ];
            }

            $this->overrideViews($app, 'core');
        }
        

        
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@kfit/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }
}
