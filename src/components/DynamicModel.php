<?php

namespace kfit\core\components;

/**
 * Clase base para los modelos dinámicos.
 *
 * @package kfit
 * @subpackage core/components
 * @category Components
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class DynamicModel extends \yii\base\DynamicModel
{
    protected $_labels;

    public function setAttributeLabels(array $labels = [])
    {
        $this->_labels = $labels;
    }

    public function getAttributeLabel($name)
    {
        return $this->_labels[$name] ?? $name;
    }

    /**
     * Crear una intancia de la clase con los attributos y las reglas ya pasadas y listo para utilizar.
     *
     * @param array $attributes Atributos a ser validados.
     * @param array $rules Reglas de validación en forma de arreglo.
     * @return DynamicModel
     */
    public static function withRules($attributes, $rules = [])
    {
        $instance = new self($attributes);
        foreach ($rules as $rule) {
            $options = [];
            if (count($rule) > 2) {
                $options = $rule;
                unset($options[0], $options[1]);
            }
            $instance->addRule($rule[0], $rule[1], $options);
        }
        return $instance;
    }
}
