<?php

namespace kfit\core\widgets\fileinput;

/**
 * Widget para inputs tipo file.
 *
 * @package kfit\core
 * @subpackage widgets\FileInput
 * @category Category
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr320@gmail.com>
 *
 */

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;


class FileInput extends Widget
{
    public $model;
    public $attribute;
    public $formName;
    public $dataPreview;
    public $multiple;
    public $_suffixFileOld = '-old';
    public $allowedFileExtensions = [];
    public $maxFilesCount;
    public $titlesInput = [];
    public $options = [];

    /**
     * Evento para inicializar el widget
     */
    public function init()
    {
        parent::init();
        if ($this->multiple == null) {
            $this->multiple = false;
        }
        $this->attribute = str_replace('[]', '', $this->attribute);
        if ($this->maxFilesCount == null || $this->maxFilesCount == 0) {
            $this->maxFilesCount = 1;
        }
        if (is_string($this->dataPreview)) {
            $this->dataPreview = [$this->dataPreview];
        }
    }

    /**
     * Evento de ejecución del widget
     * 
     * @return string
     */
    public function run()
    {
        FileInputAsset::register($this->view);
        return $this->render('main');
    }
}
