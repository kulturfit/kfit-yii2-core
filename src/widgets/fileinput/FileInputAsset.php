<?php

namespace kfit\core\widgets\fileinput;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@kfit/core/widgets/fileinput/assets';
    public $css = [
        'css/magnific-popup.css',
        'css/fileinput.css'
    ];
    public $js = [
        'js/magnific-popup.js',
        'js/fileinput.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'kfit\core\web\BaseAsset',
    ];
}
