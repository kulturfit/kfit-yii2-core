<?php


namespace kfit\core\widgets\linksorter;


use yii\helpers\Html;

/**
 * Class LinkSorter
 * @package kfit
 * @subpackage core\widgets\linksorter
 * @category widgets
 *
 * @author Kevin Daniel Guzmán Delgadillo
 *
 */
class LinkSorter extends \yii\widgets\LinkSorter
{
    /**
     * Renders the sort links.
     *
     * @return string the rendering result
     * @throws
     */
    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        return $this->render('main', [
            'attributes' => $attributes,
            'widget' => $this
        ]);
    }
}