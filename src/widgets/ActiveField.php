<?php

namespace kfit\core\widgets;

use Yii;
use yii\helpers\Html;
use yii\bootstrap4\ActiveField as BaseActiveField;
use yii\web\JsExpression;

/**
 * ActiveField representa un input de form para el [[ActiveForm]].
 *
 * @package kfit
 * @subpackage widgets
 * @category Widgets
 *
 * @property string $help Título para texto de ayuda para el tooltip
 * @property mixed $popover Texto completo de la ayuda
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class ActiveField extends BaseActiveField
{

    public $help         = null;
    public $popover;
    public $errorOptions = ['class' => 'invalid-feedback'];

    /**
     * {@inheritdoc}
     */
    public function label($label = null, $options = [])
    {
        if ($label === false) {
            $this->parts['{label}'] = '';
            return $this;
        }

        $options = array_merge($this->labelOptions, $options);
        if ($label !== null) {
            $options['label'] = $label;
        }
        $help = '';
        if (!is_null($this->help)) {
            $help .= '&nbsp;&nbsp;&nbsp;&nbsp;' . Yii::$app->html::tag(
                'span',
                $this->help,
                [
                    'style' => 'color:#BFBFBF'
                ]
            );
            if (!empty($this->popover)) {
                $help .= '&nbsp;&nbsp;' . Yii::$app->html::tag(
                    'span',
                    '',
                    [
                        'class'          => 'fa fa-info-circle',
                        'data-title'     => Yii::t('app', 'Help'),
                        'data-placement' => 'top',
                        'data-content'   => $this->popover,
                        'data-trigger'   => 'hover',
                        'data-toggle'    => 'popover',
                    ]
                );
            }
        }
        $this->parts['{label}'] = Yii::$app->html::activeLabel(
            $this->model,
            $this->attribute,
            $options
        ) . $help;

        return $this;
    }

    /**
     * Returns the JS options for the field.
     * @return array the JS options.
     */
    protected function getClientOptions()
    {
        $options = parent::getClientOptions();
        $options['id'] = $this->getInputId();

        // if ($this->attribute == 'confirmPassword') {
        //     dd($options);
        // }
        return $options;
    }
}
