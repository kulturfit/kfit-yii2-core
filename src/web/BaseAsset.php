<?php

namespace kfit\core\web;

use yii\web\AssetBundle;

/**
 * Esta Clase Administra los Assets para el plugin bootbox.js
 * @package kfit
 * @subpackage assets
 * @category Assets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class BaseAsset extends AssetBundle
{

    /**
     * @var string
     * Source base para el Asset
     */
    public $sourcePath = '@vendor/kfit/yii2-core/src/assets';

    /**
     * @var array
     * Archivos CSS
     */
    public $css = [
        'core.css'
    ];

    /**
     * @var array
     * Archivos JavaScript
     */
    public $js = [
        'core.js'
    ];

    /**
     * @var array
     * Dependencias del Asset
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'kfit\core\web\BowerAsset',
    ];

}
