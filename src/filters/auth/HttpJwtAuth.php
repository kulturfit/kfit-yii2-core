<?php

namespace kfit\core\filters\auth;

use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\auth\HttpBearerAuth;

use yii\web\UnauthorizedHttpException;
use yii\base\UnexpectedValueException;

class HttpJwtAuth extends HttpBearerAuth
{
    const TYPE_VALIDATE = 'ATK';
    const TYPE_RENEW = 'RTK';

    public $type;
    public $publicKey;
    public $privateKey;

    public function init()
    {
        parent::init();

        if (empty($this->publicKey)) {
            throw new InvalidConfigException('Error public key');
        }

        if (empty($this->privateKey)) {
            throw new InvalidConfigException('Error private key');
        }

        if (empty($this->type)) {
            $this->type = self::TYPE_VALIDATE;
        }
    }


    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {

        $authHeader = $request->getHeaders()->get($this->header);

        if ($authHeader !== null) {
            if ($this->pattern !== null) {
                if (preg_match($this->pattern, $authHeader, $matches)) {
                    $authHeader = $matches[1];
                } else {
                    return null;
                }
            }

            try {

                $decoded = JWT::decode($authHeader, $this->publicKey, array('RS512'));

                if ($decoded) {
                    $identityClass = $user->identityClass;
                    $identity = null;
                    if ($this->type === self::TYPE_VALIDATE && $decoded->type === self::TYPE_VALIDATE) {
                        $identity = $identityClass::findIdentityByAccessToken($decoded->tok, get_class($this));
                    } else if ($this->type === self::TYPE_RENEW && $decoded->type === self::TYPE_RENEW) {
                        $identity = $identityClass::findIdentityByRenewToken($decoded->tok, get_class($this));
                    }

                    if ($identity === null) {
                        $this->challenge($response);
                        $this->handleFailure($response);
                    }else{
                        $user->login($identity);
                    }
                    return $identity;
                }
            } catch (ExpiredException $ex) {
                throw new UnauthorizedHttpException($ex->getMessage(), 900);
            } catch (SignatureInvalidException $ex) {
                throw new UnauthorizedHttpException($ex->getMessage(), 901);
            } catch (BeforeValidException $ex) {
                throw new UnauthorizedHttpException($ex->getMessage(), 902);
            } catch (UnexpectedValueException $ex) {
                throw new UnauthorizedHttpException($ex->getMessage(), 903);
            } catch (Exception $ex) {
                throw new UnauthorizedHttpException($ex->getMessage(), 904);
            }

            if ($identity === null) {
                $this->challenge($response);
                $this->handleFailure($response);
            }

            return $identity;
        }

        return null;
    }
}
