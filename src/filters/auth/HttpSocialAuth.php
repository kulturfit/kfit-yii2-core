<?php

namespace kfit\core\filters\auth;

use app\models\app\SocialNetworks;
use Yii;
use yii\filters\auth\AuthMethod;
use kfit\core\models\User;
use kfit\core\models\RedesSocialesUsuario;
use yii\base\DynamicModel;

/**
 * Implementación de autenticacion normal y por redes sociales
 *
 * @package kfit
 * @subpackage filters/auth
 * @category Filters
 *
 * @property string $realm
 *
 * @author Kevin Guzmán <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class HttpSocialAuth extends AuthMethod
{

    public $realm = 'api';

    /**
     * Proceso de validación
     */
    public function authenticate($user, $request, $response)
    {
        $userObject = null;
        /* Validacion de datos */
        $model       = new DynamicModel([
            'social_network_code', 'social_network_user', 'response', 'email'
        ]);
        $model->addRule([
            'social_network_code',
            'social_network_user',
            'response',
            'email'
        ], 'required')
            ->addRule(['email'], 'email')
            ->addRule(['social_network_user'], 'string', ['max' => 128])
            ->addRule(
                ['social_network_code'],
                'in',
                ['allowArray' => true, 'range' => ['FAB', 'GOP']]
            );

        if ($model->load($request->post(), '') && $model->validate()) {
            $userObject = Yii::$app->user->identityClass::findByName($model->email);
            if ($userObject !== null) {
                if ($this->existeIdRedesSociales(
                    $model->social_network_user,
                    $model->social_network_code,
                    $userObject
                )) {
                    $user->switchIdentity($userObject);
                }
            } else {
                $userObject = $this->crearUsuario($request->post());
                $user->switchIdentity($userObject);
            }
        } else {
            throw new \yii\web\HttpException(
                401,
                Yii::t(
                    'app',
                    'Invalid authentication credentials.'
                )
            );
        }
        return $userObject;
    }

    /**
     * Valida que el token pasado es valido en las redes sociales registradas
     *
     * @param string $tokenRedSocial Identificador de la red social
     * @param string $idRedSocial tipo de red social
     * @param User $userObject Información del usuario
     * @return boolean
     */
    public function existeIdRedesSociales(
        $tokenRedSocial,
        $idRedSocial,
        $userObject
    ) {
        $encontrado    = false;
        $redesSociales = $userObject->redesSocialesUsuario;
        if (count($redesSociales) > 0) {
            foreach ($redesSociales as $redSocial) {
                if ($redSocial->social_network_user === $tokenRedSocial && $redSocial->social_network_code === $idRedSocial) {
                    $encontrado = true;
                }
            }
        }
        return $encontrado;
    }

    /**
     * Realiza la creación de un usuario y una red social segun los datos pasados
     *
     * @param array $parametrosPost parametros de la petición post
     * @return User
     */
    public function crearUsuario($parametrosPost)
    {
        $passwordHash = Yii::$app->getSecurity()->generatePasswordHash($parametrosPost['social_network_code'] . '-' . $parametrosPost['social_network_user']);
        /* Creación de usuario */
        $userObject = Yii::createObject(Yii::$app->user->identityClass);
        $userObject->load($parametrosPost, '');
        $userObject->username = $parametrosPost['social_network_user'];
        $userObject->password_hash = $passwordHash;
        $userObject->auth_key = Yii::$app->strings->randomString(32);
        $userObject->confirmed_email = 'Y';
        $userObject->blocked = 'N';
        $userObject->save(false);
        $this->crearRedSocial($userObject->primaryKey, $parametrosPost);
        /* Retorno usuario */
        return $userObject;
    }

    /**
     * Realiza la creación de la red social del usuario
     *
     * @param User $userObject
     * @return SocialNetworks
     */
    public function crearRedSocial($idUsuario, $parametrosPost)
    {
        /* Creación de red social */
        $parametrosPost = array_merge(
            $parametrosPost,
            [
                'user_id' => $idUsuario
            ]
        );
        $infoRedSocial  = new SocialNetworks();
        $infoRedSocial->load($parametrosPost, '');
        $infoRedSocial->save(false);
        /* Retorno Red Social */
        return $infoRedSocial;
    }
}
