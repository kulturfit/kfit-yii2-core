<?php

namespace kfit\core\rest;

use kfit\core\filters\auth\HttpJwtAuth;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpBasicAuth;

/**
 * ActiveController Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package kfit
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class ActiveController extends \yii\rest\ActiveController
{

    /**
     * Permite definir el modelo para las busquedas
     * @var type
     */
    public $searchModel;

    /**
     * Llave primaria del modelo para la sincronización
     * @var array
     */
    public $primaryKey;

    /**
     * Permite definir la estructura para procesar los datos con campos adicionales
     * @var array
     */
    public $processStructure = [];

    /**
     * La configuración para crear el serializador para el formato de respuesta.
     * @var array
     */
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $response         = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        Yii::$app->user->enableSession = false;

        if ($this->searchModel === null) {
            throw new InvalidConfigException('The "searchModel" property must be set.');
        }
    }

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $this->module->cors();

        $behaviors                                                     = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className()
            ],
        ];

        if (isset(Yii::$app->params['privateKey'])) {

            unset($behaviors['authenticator']);

            // $behaviors['bearer'] = [
            //     'class' => HttpBearerAuth::className(),
            //     'except' => ['renew-jwt']
            // ];

            $behaviors['jwt'] = [
                'class' => HttpJwtAuth::className(),
                'except' => ['renew-jwt'],
                'type' => HttpJwtAuth::TYPE_VALIDATE,
                'privateKey' => Yii::$app->params['privateKey'],
                'publicKey' => Yii::$app->params['publicKey'],
            ];
        }




        return $behaviors;
    }

    /**
     * Retorna la parametrización para las acciones por defecto
     *
     * @return array
     */
    public function actions()
    {
        $actions            = parent::actions();
        $actions['delete']  = [
            'class'       => 'kfit\core\rest\actions\DeleteAction',
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];
        $actions['restore'] = [
            'class'       => 'ticmkaers\core\rest\actions\RestoreAction',
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        if (!empty($this->primaryKey)) {
            $actions['sync'] = [
                'class'       => 'kfit\core\rest\actions\SyncAction',
                'modelClass'  => $this->modelClass,
                'searchModel' => $this->searchModel,
                'primaryKey'  => $this->primaryKey,
                'checkAccess' => [$this, 'checkAccess'],
            ];

            if (!empty($this->processStructure)) {
                $actions['sync']['class']            = 'kfit\core\rest\actions\SyncActionMultiple';
                $actions['sync']['processStructure'] = $this->processStructure;
            }

            $actions['create-all'] = [
                'class'       => 'kfit\core\rest\actions\CreateAllAction',
                'modelClass'  => $this->modelClass,
                //                'searchModel' => $this->searchModel,
                'primaryKey'  => $this->primaryKey,
                'checkAccess' => [$this, 'checkAccess'],
            ];
        }

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Retorna el DataProvider para la acction index
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searchModel = Yii::createObject($this->searchModel);

        return $searchModel->search(Yii::$app->request->queryParams);
    }

    /**
     * Declara los verbos HTTP permitidos.
     *
     * @return array
     */
    public function verbs()
    {
        $verbs           = parent::verbs();
        $verbs["index"]  = ['GET'];
        $verbs["update"] = ['PUT', 'PATCH', 'POST'];
        $verbs["delete"] = ['PUT', 'PATCH'];
        return $verbs;
    }
}
