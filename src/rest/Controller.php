<?php

namespace kfit\core\rest;

use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use kfit\core\filters\auth\HttpBasicAuth;
use kfit\core\filters\auth\HttpJwtAuth;
use Yii;
use yii\filters\auth\HttpBearerAuth;

/**
 * Controller Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package kfit
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Controller extends \yii\rest\Controller
{

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $this->module->cors();


        $behaviors                                                     = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => HttpBasicAuth::className(),
            // 'authMethods' => [
            //     HttpBasicAuth::className(),
            //     // HttpBearerAuth::className()
            // ],
            'except' => ['renew-jwt'],
        ];

        if (isset(Yii::$app->params['privateKey'])) {

        //     $behaviors['jwt'] = [
        //         'class' => HttpJwtAuth::className(),
        //         'except' => ['renew-jwt'],
        //         'type' => HttpJwtAuth::TYPE_VALIDATE,
        //         'privateKey' => Yii::$app->params['privateKey'],
        //         'publicKey' => Yii::$app->params['publicKey'],
        //     ];

            $behaviors['renew-jwt'] = [
                'class' => HttpJwtAuth::className(),
                'only' => ['renew-jwt'],
                'type' => HttpJwtAuth::TYPE_RENEW,
                'privateKey' => Yii::$app->params['privateKey'],
                'publicKey' => Yii::$app->params['publicKey'],
            ];
        }




        return $behaviors;
    }

}
