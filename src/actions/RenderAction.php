<?php

namespace kfit\core\actions;

use Yii;
use kfit\core\actions\BaseAction;
/*
 * Acción para renderizar una vista
 *
 * @package kfit\core\actions\RenderAction
 * 
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com
 */

class RenderAction extends BaseAction
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    public $paramsRender = [];

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Permite renderizar una vista
     *
     * @return mixed
     */
    public function run()
    {
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $this->paramsRender = Yii::$app->arrayHelper::merge($this->paramsRender, $paramsGet);
        }
        if ($this->_viewFile) {
            if (!empty($this->beforeRender) && is_callable($this->beforeRender)) {
                call_user_func_array($this->beforeRender, [&$this->_viewFile, &$this->paramsRender]);
            }
            $output = $this->render(
                $this->_viewFile,
                $this->paramsRender
            );
        } else {
            $output = $this->controller->renderContent('No defined viewFile');
        }
        return $output;
    }
}
