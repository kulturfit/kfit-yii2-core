<?php

namespace kfit\core\actions;

use Yii;
use kfit\core\actions\BaseAction;

/**
 * ViewAction base para los crud del sistema
 *
 * @package kfit\core\actions\ViewAction
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ViewAction extends BaseAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'view';

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @return void
     */
    public function run($id)
    {
        $params = [
            'model' => $this->findModel($id),
        ];

        $this->runCallback($this->beforeRender, [&$params]);

        return $this->render($this->_viewFile, $params);
    }
}
