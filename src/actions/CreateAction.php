<?php

namespace kfit\core\actions;

use Yii;

/**
 * CreateAction base para los crud del sistema
 *
 * @package kfit
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class CreateAction extends SaveAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'create';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'It was created successfully.';
}
