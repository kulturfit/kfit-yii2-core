<?php

namespace kfit\core\actions;

use Yii;
use kfit\core\actions\BaseAction;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;

/**
 * ViewAction base para los crud del sistema
 *
 * @package kfit\core\actions
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class SlugViewAction extends BaseAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'view';

    /**
     * Nombre del atributo a usar para la busqueda del registro
     *
     * @var string
     */
    public $slugAttribute = 'slug';


    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @return void
     */
    public function run($slug)
    {

        $modelClass = $this->modelClass;
        if (is_array($this->modelClass)) {
            $primaryClassFound = false;
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if (isset($modelConfig['isPrimary']) && $modelConfig['isPrimary']) {
                    $modelClass = $modelConfig['class'];
                    $primaryClassFound = true;
                    break;
                }
            }
            if (!$primaryClassFound) {
                throw new InvalidConfigException(Yii::t('app', 'Primary model not found'));
            }
        }


        $model = $modelClass::find()
            ->where([
                $this->slugAttribute => Inflector::slug($slug),
                $modelClass::STATUS_COLUMN => $modelClass::STATUS_ACTIVE
            ])
            ->one();

        $params = ['model' => $model];

        if ($model !== null) {
            return $this->render($this->_viewFile, $params);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
