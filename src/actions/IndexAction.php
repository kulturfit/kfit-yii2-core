<?php

namespace kfit\core\actions;

use Yii;
use kfit\core\actions\BaseAction;

/**
 * Acción base para los index del los crud del sistema
 *
 * @package kfit
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class IndexAction extends BaseAction
{

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'index';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $renderAjax = false;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSearch;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isSortableGrid = false;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $searchMethod = 'search';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $titleInGrid = true;
    /**
     * Lista todos registros según el DataProvider.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            'isSortableGrid' => $this->isSortableGrid,
            'titleInGrid' => $this->titleInGrid
        ];
        if (!empty($this->modelClass)) {
            $modelObject = Yii::createObject($this->modelClass);
            $paramsGet = Yii::$app->request->get();
            if (!empty($paramsGet)) {
                $modelObject->setAttributes($paramsGet);
            }

            $this->runCallback($this->beforeSearch, [&$modelObject]);

            //@todo Remover callback controller
            $this->controller->beforeSearchIndex($modelObject);

            $dataProvider = $this->runCallback([$modelObject, $this->searchMethod], [Yii::$app->request->queryParams]);
            $params = [
                'isSortableGrid' => $this->isSortableGrid,
                'titleInGrid' => $this->titleInGrid,
                'searchModel' => $modelObject,
                'dataProvider' => $dataProvider,
            ];
        }

        $this->runCallback($this->beforeRender, [&$this->_viewFile, &$params]);

        //@todo Remover callback controller
        $this->controller->beforeRenderIndex($this->_viewFile, $params);

        return $this->render($this->_viewFile, $params);
    }
}
