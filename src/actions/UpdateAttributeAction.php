<?php

namespace kfit\core\actions;

use Yii;
use kfit\core\actions\BaseAction;
use kfit\core\widgets\ActiveForm;

/**
 * UpdateAttributeAction base para los crud del sistema
 *
 * @package kfit\core\actions\UpdateAttributeAction
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class UpdateAttributeAction extends BaseAction
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSave;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $value;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $attribute;

    /**
     * Permite actualizar el valor de un atributo de un modelo en el sistema
     *
     * @return void
     */
    public function run($id)
    {
        $response = null;
        $modelInstance = $this->findModel($id);
        $modelInstance->{$this->attribute} = $this->value;

        if (!empty($this->beforeSave) && is_callable($this->beforeSave)) {
            call_user_func_array($this->beforeSave, [&$modelInstance]);
        }
        if ($modelInstance->save(true, [$this->attribute])) {
            if ($this->isModal) {
                $response = [
                    'state' => Yii::$app->message::TYPE_SUCCESS,
                    'message' => Yii::t('app', $this->messageOnSuccess),
                    'errors' => '',
                    'type' => 'message', //open-modal, open-load-modal, redirect, message
                    'url' => '',
                    'modal' => ''
                ];
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', $this->messageOnSuccess));
            }
        } else {
            if ($this->isModal) {
                $response = [
                    'state' => Yii::$app->message::TYPE_DANGER,
                    'message' => Yii::$app->html::errorSummary(
                        $modelInstance
                    ),
                    'errors' => ActiveForm::validate($modelInstance),
                    'type' => 'message', //open-modal, open-load-modal, redirect, message
                    'url' => '',
                    'modal' => ''
                ];
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', $this->messageOnError));
            }
        }

        if ($this->redirectOnSuccess) {
            $response = $this->redirect([$modelInstance]);
        }

        return $response;
    }
}
